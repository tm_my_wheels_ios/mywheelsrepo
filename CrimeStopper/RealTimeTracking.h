//
//  RealTimeTracking.h
//  CrimeStopper
//
//  Created by Sonali on 27/10/16.
//  Copyright © 2016 Emgeesons. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RealTimeTracking : UIView

@property (strong, nonatomic) IBOutlet UIButton *imInterestedButton;



@property (strong, nonatomic) IBOutlet UIButton *noThanksButton;

@property (strong, nonatomic) IBOutlet UIButton *cancelButton;

@property (strong, nonatomic) IBOutlet UIView *RealTimeTrackingView;
@property (nonatomic,retain) NSMutableData *receivedData;


@end
