//
//  ReportSummaryViewController.h
//  CrimeStopper
//
//  Created by Yogesh Suthar on 17/07/2014.
//  Copyright (c) 2014 Emgeesons. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ReportSummaryViewController : UIViewController

@property (nonatomic, strong) NSArray *detailsArray;

@end
