//
//  MyVehicleCell.m
//  CrimeStopper
//
//  Created by Asha Sharma on 03/07/14.
//  Copyright (c) 2014 Emgeesons. All rights reserved.
//

#import "MyVehicleCell.h"

@implementation MyVehicleCell

- (void)awakeFromNib
{
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
