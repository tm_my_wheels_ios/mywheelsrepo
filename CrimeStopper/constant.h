//
//  constant.h
//  CrimeStopper
//
//  Created by Asha Sharma on 06/06/14.
//  Copyright (c) 2014 Emgeesons. All rights reserved.
//

#ifndef ShoppingCart_Header_h
#define ShoppingCart_Header_h

#define SERVERNAME   @"https://www.mywheels.biz/mobile2.0/"
//#define SERVERNAME   @"http://www.emgeesonsdevelopment.in/crimestoppers/mobile2.0/"

#define OS_VERSION [[UIDevice currentDevice] systemVersion]

#define MAKE [UIDevice currentDevice].model

#endif
