//
//  VehicleTipsCell.h
//  CrimeStopper
//
//  Created by Asha Sharma on 14/07/14.
//  Copyright (c) 2014 Emgeesons. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface VehicleTipsCell : UITableViewCell

@property (nonatomic,retain) IBOutlet UILabel *lblTips;
@property (nonatomic,retain) IBOutlet UILabel *lblDate;

@end
