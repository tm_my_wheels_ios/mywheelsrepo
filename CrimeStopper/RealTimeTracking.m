//
//  RealTimeTracking.m
//  CrimeStopper
//
//  Created by Sonali on 27/10/16.
//  Copyright © 2016 Emgeesons. All rights reserved.
//

#import "RealTimeTracking.h"
#import "Reachability.h"
//#import "SVProgressHUD.h"

@implementation RealTimeTracking

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/




- (IBAction)imInterestedButton:(id)sender
{


    Reachability *networkReachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus networkStatus = [networkReachability currentReachabilityStatus];
    if (networkStatus == NotReachable)
    {
        [DeviceInfo errorInConnection];
        return;
    }
    
   
   
   // [SVProgressHUD showWithMaskType:SVProgressHUDMaskTypeBlack];
    
    // URL
    NSString *url = [NSString stringWithFormat:@"http://www.mywheels.biz/mobile2.0/updateInterest.php", SERVERNAME];
    
    //UserId
    NSString *userId = [[NSUserDefaults standardUserDefaults] objectForKey:@"UserID"];
    
   
    
    //isInterested
    
    NSString *isInterested = @"1";
   // NSString *isInterested = [[NSUserDefaults standardUserDefaults] objectForKey:@"isInterested"];

    
    NSArray *keys = [[NSArray alloc]initWithObjects:@"userId", @"isInterested", nil];


    NSArray *values =[[NSArray alloc]initWithObjects:userId,isInterested,nil];

    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    
    NSURL *baseUrl = [NSURL URLWithString:url];
    
    NSString *charset = (NSString *)CFStringConvertEncodingToIANACharSetName(CFStringConvertNSStringEncodingToEncoding(NSUTF8StringEncoding));
    [request setURL:baseUrl];
    [request setHTTPMethod:@"POST"];
    
    NSString *boundary = @"0xKhTmLbOuNdArY";
    NSString *endBoundary = [NSString stringWithFormat:@"\r\n--%@\r\n", boundary];
    
    NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; charset=%@; boundary=%@", charset, boundary];
    [request addValue:contentType forHTTPHeaderField: @"Content-Type"];
    
    NSMutableData *tempPostData = [NSMutableData data];
    [tempPostData appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    


    for(int i=0;i<keys.count;i++)
    {
        NSString *str = values[i];
        NSString *key =keys[i];
        NSLog(@"Key Value pair: %@-%@",key,str);
        [tempPostData appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\"\r\n\r\n", key] dataUsingEncoding:NSUTF8StringEncoding]];
        [tempPostData appendData:[str dataUsingEncoding:NSUTF8StringEncoding]];
        // [tempPostData appendData:[@"\r\n--%@\r\n",boundary dataUsingEncoding:NSUTF8StringEncoding]];
        [tempPostData appendData:[endBoundary dataUsingEncoding:NSUTF8StringEncoding]];
        
    }
    
    
    
    [tempPostData appendData:[[NSString stringWithFormat:@"\r\n--%@--\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    [request setHTTPBody:tempPostData];
    _receivedData = [NSMutableData dataWithCapacity: 0];
    
    
    NSURLConnection *theConnection = [[NSURLConnection alloc] initWithRequest:request delegate:self];
    if( theConnection )
    {
        
        NSLog(@"request uploading successful");
        
        // storing bool value (click on I am interested Button )
        
        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"clickOnIamInterestedButton"];
        
    }
    else
    {
        _receivedData = nil;
        NSLog(@"theConnection is NULL");
    }
 
} // imInterstedButton close




- (IBAction)noThanksButtonAction:(id)sender
{
   
    
    Reachability *networkReachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus networkStatus = [networkReachability currentReachabilityStatus];
    if (networkStatus == NotReachable)
    {
        [DeviceInfo errorInConnection];
        return;
    }
    
    
    //sending data with multiple image to server in ios8
   // [SVProgressHUD showWithMaskType:SVProgressHUDMaskTypeBlack];
    
    // URL
    NSString *url = [NSString stringWithFormat:@"http://www.mywheels.biz/mobile2.0/updateInterest.php", SERVERNAME];
    
    //UserId
    NSString *userId = [[NSUserDefaults standardUserDefaults] objectForKey:@"UserID"];
    
    
    
    //isInterested
    
    NSString *isInterested = @"0";
    // NSString *isInterested = [[NSUserDefaults standardUserDefaults] objectForKey:@"isInterested"];
    
    
    NSArray *keys = [[NSArray alloc]initWithObjects:@"userId", @"isInterested", nil];
    
    
    NSArray *values =[[NSArray alloc]initWithObjects:userId,isInterested,nil];
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    
    NSURL *baseUrl = [NSURL URLWithString:url];
    
    NSString *charset = (NSString *)CFStringConvertEncodingToIANACharSetName(CFStringConvertNSStringEncodingToEncoding(NSUTF8StringEncoding));
    [request setURL:baseUrl];
    [request setHTTPMethod:@"POST"];
    
    NSString *boundary = @"0xKhTmLbOuNdArY";
    NSString *endBoundary = [NSString stringWithFormat:@"\r\n--%@\r\n", boundary];
    
    NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; charset=%@; boundary=%@", charset, boundary];
    [request addValue:contentType forHTTPHeaderField: @"Content-Type"];
    
    NSMutableData *tempPostData = [NSMutableData data];
    [tempPostData appendData:[[NSString stringWithFormat:@"--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    
    
    
    for(int i=0;i<keys.count;i++)
    {
        NSString *str = values[i];
        NSString *key =keys[i];
        NSLog(@"Key Value pair: %@-%@",key,str);
        [tempPostData appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\"\r\n\r\n", key] dataUsingEncoding:NSUTF8StringEncoding]];
        [tempPostData appendData:[str dataUsingEncoding:NSUTF8StringEncoding]];
        // [tempPostData appendData:[@"\r\n--%@\r\n",boundary dataUsingEncoding:NSUTF8StringEncoding]];
        [tempPostData appendData:[endBoundary dataUsingEncoding:NSUTF8StringEncoding]];
        
    }
    
    
    
    [tempPostData appendData:[[NSString stringWithFormat:@"\r\n--%@--\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    [request setHTTPBody:tempPostData];
    _receivedData = [NSMutableData dataWithCapacity: 0];
    
    
    NSURLConnection *theConnection = [[NSURLConnection alloc] initWithRequest:request delegate:self];
    if( theConnection )
    {
        
        // storing bool value (click on No thanks Button) (i have used same for key name for both buttons)
        
        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"clickOnIamInterestedButton"];
        
        NSLog(@"request uploading successful");
        
        
    }
    else
    {
        _receivedData = nil;
        NSLog(@"theConnection is NULL");
    }
 
    
}




#pragma mark nsurlconnection delegate methods

- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response
{
    
    [_receivedData setLength:0];
    NSLog(@"responsse : %@",response);
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data
{
    
    [_receivedData appendData:data];
    NSLog(@"receive data : %@",_receivedData);
}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection {
    NSLog(@"connectionDidFinishLoading");
    NSLog(@"Succeeded! Received %ld bytes of data",[self.receivedData length]);
    // NSString *strr = [[NSString alloc] initWithData:self.receivedData encoding:NSUTF8StringEncoding];
    NSLog(@"data is: %@",self.receivedData);
    
    //NSDictionary *dict = [[NSDictionary alloc] initwithd]
    
    // convert to JSON
    
    NSError *e = nil;
    NSArray *jsonArray = [NSJSONSerialization JSONObjectWithData: _receivedData options:NSJSONReadingMutableContainers error:&e];
    
    NSDictionary *jsonDictionary = [NSJSONSerialization JSONObjectWithData:self.receivedData options:NSJSONReadingMutableLeaves error:nil];
    //  NSLog(@"data -- %@",[dict objectForKey:@"data"]);
    NSLog(@"data -- %@",jsonDictionary);
    
    NSString *EntityID = [jsonDictionary valueForKey:@"status"];
    //NSLog(@"message %@",EntityID);
    NSString *userId = [[NSUserDefaults standardUserDefaults] objectForKey:@"UserID"];
   
    
    [self removeFromSuperview];

//    if ([EntityID isEqualToString:@"Success"])
//    {
//        
//        NSLog(@"success");
//        
//        [SVProgressHUD dismiss];
//        
//        
//        
//        NSDictionary *response = (NSDictionary *)[jsonDictionary objectForKey:@"response"][0];
//        
//        
//        [SVProgressHUD dismiss];
//    } else {// this is for registered user
//        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil message:[jsonDictionary objectForKey:@"message"] delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
//        [alert show];
//        [SVProgressHUD dismiss];
//    }
//    
//    if (!jsonArray) {
//        NSLog(@"Error parsing JSON: %@", e);
//    }
}






- (IBAction)cancelButtonAction:(id)sender
{
    
    [[NSUserDefaults standardUserDefaults] setInteger: 1 forKey:@"showRealTimeTrackingView"];
     [self removeFromSuperview];
 
    
}

- (void)awakeFromNib
{
   
    [self.noThanksButton.layer setBorderWidth:1.0];
    [self.noThanksButton.layer setBorderColor:[[UIColor colorWithRed:0/255.0 green:101/255.0 blue:179/255.0 alpha:1] CGColor]];
    
    _noThanksButton.layer.cornerRadius = 2;
    
    _imInterestedButton.layer.cornerRadius = 2;
        
    
}





@end
