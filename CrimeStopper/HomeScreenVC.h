//
//  HomeScreenVC.h
//  CrimeStopper
//
//  Created by Asha Sharma on 05/06/14.
//  Copyright (c) 2014 Emgeesons. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

@interface HomeScreenVC : UIViewController
{
    NSTimer *timer;
}
@property (nonatomic,retain) NSString *str;
@property (nonatomic) NSInteger *ShowViewCount;
@property (nonatomic,retain) NSString *RealTimeTracking;

-(IBAction)btn_lik:(id)sender;
@end
