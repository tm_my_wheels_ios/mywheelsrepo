//
//  ShareNewReportViewController.h
//  CrimeStopper
//
//  Created by Yogesh Suthar on 07/07/2014.
//  Copyright (c) 2014 Emgeesons. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ShareNewReportViewController : UIViewController

@property (nonatomic, strong) NSString *photo1, *vehicleId;
@property (strong, nonatomic) IBOutlet UIToolbar *toolBar;

@end
