//
//  mParkingCell.h
//  CrimeStopper
//
//  Created by Asha Sharma on 07/07/14.
//  Copyright (c) 2014 Emgeesons. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface mParkingCell : UITableViewCell
@property (nonatomic,retain) IBOutlet UILabel *lblCheckList;
@property (nonatomic,retain) IBOutlet UIImageView *imgCheck;

@end
